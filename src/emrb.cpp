#include "emrb/emrb.h"
#include <iostream>
#include <chrono>
#include <thread>
#include <iomanip>

using namespace std;

emrb::emrb(){
  m_ADCChannel = 0;
  m_MultChannel = 0;
  m_channelNumber = 0;
}

emrb::~emrb (){
  delete m_EnCon;
  delete m_SPICon;
}

void emrb::Init(){
  // Open the SPI connection
  m_EnCon = MultControl::GetInstance();
  m_SPICon = SPIControl::GetInstance();  
  m_SPICon->Open();
  // Check if SPI connection is open
  if(not m_SPICon->IsOpen()){
    cout << "SPI is not open, terminating" << endl;
    return;
  }
  // Initialize the ADC
  m_SPICon->ADC_Init();
  // Setup ADC for single read mode
  m_SPICon->ADC_Setup_Read_Single();  
}

uint8_t emrb::SelectChannel(uint32_t chan){
  std::pair<uint8_t, uint8_t> pinSelected = m_SPICon->ADC_Read_Pin(chan);
  m_EnCon->SetEnable(pinSelected.first);  
  if (pinSelected.first == -1 && pinSelected.second == -1) {
    std::cerr << " Error : no pin selected" << std::endl;
    return static_cast<uint8_t>(-1);  
  }
  m_channelNumber = pinSelected.second;
  return pinSelected.second;
}

uint32_t emrb::ReadRawValue() {
  return m_SPICon->ADC_Read_Single(m_channelNumber);
} 

double emrb::ReadVoltageValue() {
  return CalibrateV(ReadRawValue());
}

double emrb::CalibrateV(uint32_t adc) {
  double volts=((float)(adc>>8)/0xffff) * 2.5;
  //cout << "Voltage = " << fixed << setprecision(2) << volts << " V" << endl;
  return volts;
}

double emrb::ReadTemperatureValue() {
  double volts = ReadVoltageValue();
  return CalibrateT(volts);
} 

double emrb::CalibrateT(double volts){
  double celsius=0;
  //for(uint32_t i=0;i<m_calib[gain].size();i++){
  //celsius += m_calib[gain][i]*pow(volts,i);
  //}
  return celsius;
}

