#include "emrb/MultControl.h"
#include <chrono>
#include <iostream>
#include <thread>


using namespace std;
int main(){


  MultControl * Controller = MultControl::GetInstance(); 
  
  //Run forever slowly blinking different GPIO pins
  while(true){
    for(uint8_t EnInd=0;EnInd<32;EnInd++){
      Controller->SetEnable(EnInd);
      std::cout<<"Enable Signal set to: "<<int(EnInd)<<std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    }

  }
  return 0;
}
