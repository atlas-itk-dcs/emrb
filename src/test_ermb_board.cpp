#include "emrb/MultControl.h"
#include "emrb/SPIControl.h"
#include <chrono>
#include <iostream>
#include <thread>


using namespace std;
int main(){


  MultControl * EnCon = MultControl::GetInstance();
  SPIControl * SPICon = SPIControl::GetInstance(); 

  SPICon->Open();
  if(not SPICon->IsOpen()){
    std::cout<<"SPI is not open, terminating"<<std::endl;
    return 0;
  }
  SPICon->ADC_Init();
  SPICon->ADC_Setup_Read_Single();

 


  unsigned long RegisterRead = SPICon->ADC_Read_Config_Register();
  std::cout<<"Communication Check: Reading Config Register "<<std::hex<<(uint64_t(RegisterRead))<<std::dec<<std::endl;















  
  //Run forever slowly swittich different Mult selection bins
  while(true){
    for(uint8_t EnInd=0;EnInd<4;EnInd++){
      EnCon->SetEnable(EnInd);
      std::this_thread::sleep_for(std::chrono::seconds(3));
      for(uint8_t ADCChn=0;ADCChn<4;ADCChn++){

	unsigned long retVal = SPICon->ADC_Read_Single(ADCChn);
	std::cout<<"Mult: "<<int(EnInd)<<" Chn: "<<int(ADCChn)<<" ADC Reading: "<<std::hex<<retVal<<std::dec<<std::endl;
      }

    }

  }
  return 0;
}
