#include "emrb/CS5523.h"
#include <iostream>
#include <vector>

int main(){

  CS5523 m_ADC;

  std::vector<CS5523::Field> Fields ={
    m_ADC.CHOP_FREQUENCY_SELECT,
    m_ADC.MULTIPLE_CONVERSION,
    m_ADC.LOOP,
    m_ADC.READ_CONVERT,
    m_ADC.DEPTH_POINTER,
    m_ADC.POWER_SAVE_SELECT,
    m_ADC.PUMP_DISABLE,
    m_ADC.RUN,
    m_ADC.LOW_POWER_MODE,
    m_ADC.RESET_SYSTEM,
    m_ADC.RESET_VALID,
    m_ADC.OSCILLATION_DETECT,
    m_ADC.OVERRANGE_FLAG,
    m_ADC.LATCH_OUTPUT_1,
    m_ADC.CHANNEL_SELECT_1,
    m_ADC.WORD_RATE_1,
    m_ADC.GAIN_BITS_1,
    m_ADC.UNIPOLAR_MODE_1,

    m_ADC.LATCH_OUTPUT_2,
    m_ADC.CHANNEL_SELECT_2,
    m_ADC.WORD_RATE_2,
    m_ADC.GAIN_BITS_2,
    m_ADC.UNIPOLAR_MODE_2,

    m_ADC.LATCH_OUTPUT_3,
    m_ADC.CHANNEL_SELECT_3,
    m_ADC.WORD_RATE_3,
    m_ADC.GAIN_BITS_3,
    m_ADC.UNIPOLAR_MODE_3,
    m_ADC.LATCH_OUTPUT_4,
    m_ADC.CHANNEL_SELECT_4,
    m_ADC.WORD_RATE_4,
    m_ADC.GAIN_BITS_4,
    m_ADC.UNIPOLAR_MODE_4,

    m_ADC.LATCH_OUTPUT_5,
    m_ADC.CHANNEL_SELECT_5,
    m_ADC.WORD_RATE_5,
    m_ADC.GAIN_BITS_5,
    m_ADC.UNIPOLAR_MODE_5,
  
    m_ADC.LATCH_OUTPUT_6,
    m_ADC.CHANNEL_SELECT_6,
    m_ADC.WORD_RATE_6,
    m_ADC.GAIN_BITS_6,
    m_ADC.UNIPOLAR_MODE_6,

    m_ADC.LATCH_OUTPUT_7,
    m_ADC.CHANNEL_SELECT_7,
    m_ADC.WORD_RATE_7,
    m_ADC.GAIN_BITS_7,
    m_ADC.UNIPOLAR_MODE_7,

    m_ADC.LATCH_OUTPUT_8,
    m_ADC.CHANNEL_SELECT_8,
    m_ADC.WORD_RATE_8,
    m_ADC.GAIN_BITS_8,
    m_ADC.UNIPOLAR_MODE_8,

    m_ADC.CH_0_SIGN,
    m_ADC.CH_0_OFFSET,
    m_ADC.CH_0_GAIN,

    m_ADC.CH_1_SIGN,
    m_ADC.CH_1_OFFSET,
    m_ADC.CH_1_GAIN,

    m_ADC.CH_2_SIGN,
    m_ADC.CH_2_OFFSET,
    m_ADC.CH_2_GAIN,

    m_ADC.CH_3_SIGN,
    m_ADC.CH_3_OFFSET,
    m_ADC.CH_3_GAIN
  
  };

uint32_t BitMask[25]={0};

for (unsigned ind=1;ind<25;ind++){
  BitMask[ind]=(BitMask[ind-1] | (0x1<<(ind-1)));
 }



 m_ADC.SetField(m_ADC.RESET_SYSTEM,1); 
 
 std::vector<uint8_t> CMD = m_ADC.GetCmd_WriteReg(m_ADC.m_reg_config);

 std::cout<<"CMD Reset System: ";
 for(unsigned i=0; i<CMD.size();i++){

   std::cout<<std::hex<<int(CMD.at(i))<<std::dec<<" ";
 }
 std::cout<<std::endl;


 CMD = m_ADC.GetCmd_ReadReg(m_ADC.m_reg_config);

 std::cout<<"CMD Read Reg: ";
 for(unsigned i=0; i<CMD.size();i++){

   std::cout<<std::hex<<int(CMD.at(i))<<std::dec<<" ";
 }
 std::cout<<std::endl;

  
 // std::cout<<"Cheking read write of each field"<<std::endl;
 //  for (uint16_t ind=0; ind<Fields.size(); ind++){

 //    m_ADC.SetField(Fields.at(ind),ind);
 //    uint32_t Val = m_ADC.GetField(Fields.at(ind));

 //    if(Val != (ind & (BitMask[Fields.at(ind).size]))){
 //      std::cout<<"Field :"<<Fields.at(ind).name<<" doesn't match the stored value: "<<int(ind)<<" -> "<< int( (ind & (BitMask[Fields.at(ind).size]) )) <<" = "<< uint32_t(Val) <<" Size: "<<int(Fields.at(ind).size) <<std::endl;
 //    }
 //  }

 //  std::cout<<"Double Cheking read write of each field"<<std::endl;
 //  for (uint16_t ind=0; ind<Fields.size(); ind++){

 //    uint32_t Val = m_ADC.GetField(Fields.at(ind));

 //    if(Val != (ind & (BitMask[Fields.at(ind).size]))){
 //      std::cout<<"Field :"<<Fields.at(ind).name<<" doesn't match the stored value: "<<int(ind)<<" -> "<< int( (ind & (BitMask[Fields.at(ind).size]) )) <<" = "<< uint32_t(Val) <<" Size: "<<int(Fields.at(ind).size) <<std::endl;
 //    }
 //  }


  






















  return 0;
}
