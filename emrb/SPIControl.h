#ifndef SPICONTROL_H
#define SPICONTROL_H

#include <stdexcept>
#include <utility>
#include <stdint.h>
#include <map>
#include <vector>
#include <mutex>
#include <unistd.h>
#include <string>
#include "emrb/CS5523.h"

/**
 *
 * @brief Multiplex control tool driven from the LED control for Reliance box code.
 * @author ismet.siral@cern.ch
 * @author carlos.solans@cern.ch
 * @author ignacio.asensi@cern.ch
 * @date Feb 2024
 **/

class SPIControl {

public:

  /**
   * Close the communication if any
   **/
  ~SPIControl();
  
  /**
   * Get the single instance
   **/
  static SPIControl * GetInstance();
 
  /**
   * Get the verbose mode
   * @return True if the verbose mode is enabled
   */
  bool GetVerbose();

  /**
   * Set the verbose mode
   * @param enable Enable the verbose mode if True
   */
  void SetVerbose(bool enable);

  /**
   * Opens the PSI object for controlling
   * @return false is something wrong happens
   **/
  bool Open();
  /**
   * Closes the PSI object for controlling
   **/
  void Close();

  
  /**
     * Check if the PSI object for controlling
     * @return returns the status
   **/
  bool IsOpen();


  /**
   * Reset/Initialize the ADC
   **/
  void ADC_Init();

  /**
   * Configure the the given CSR
   * @param CSR, Channel Select Register Bits (1to8)
   * @param Gain, Gain of the given channel
   * @param WordRage, Sampling Rate of a given channel
   * @Chn, Channel
   * @Send, Send the register. If writting multiple CSR, we recommend calling send as the last one
   * @param Data load to be written into given register set 
   **/
  
  void ADC_Configure_CSR(uint8_t CSR, uint8_t Gain, uint8_t WordRate, uint8_t Chn, bool Send=1);

  /**
   * Function to set adc to it does single bit readout *Default*
   **/
  void ADC_Setup_Read_Single();

  /**
   * Function to set adc to it does all CSR readouts
   **/

  void ADC_Setup_Read_Mult();

  /**
   *Function to readback config register it's a good way to check for communications
   **/
  unsigned long ADC_Read_Config_Register();

  /**
   *Function to readback channel setup register it's a good way to check for communications
   **/
    unsigned long ADC_Read_chanSetup();
  
  /**
   * Readout the given CSR channel, make sure ADC_Setup_Read_Single is called before
   * @param CSR, readout the given CSR which readouts the chn set inside it's register with ADC_Configure_CSR setting
   * @return return the incoming data
   **/


    unsigned long ADC_Read_CSR_Register();


  
  unsigned long ADC_Read_Single(uint8_t CSR);


  /**
   * Readout all the given CSR channels, make sure ADC_Setup_Read_Mult is called before
   * @return return the incoming data
   **/


  unsigned long ADC_Read_Mult();

  
  /**
   * ADC Set Gain
   * @param Chn, which channel
   * @param Gain, the gain set to the channel
   **/

  void ADC_Set_Gain(uint8_t chn, uint32_t Gain);


  /**
   * ADC Set Sign
   * @param Chn, which channel
   * @param Sign, the gain set to the channel
   **/

  
  void ADC_Set_Sign(uint8_t chn, uint8_t sign);

  /**
   * ADC Set Offset
   * @param Chn, which channel
   * @param Offset, the gain set to the channel
   **/

  
  void ADC_Set_Offset(uint8_t chn, uint32_t Offset);



    // Declare the ADC_Read_Pin function
    std::pair <uint8_t, uint8_t> ADC_Read_Pin(uint8_t pin);


    // Declare the Conv_Voltage_Hex_Dec function

    float Conv_Voltage_Hex_Dec(unsigned long hexValue);


 private:

  /**
   * Create an empty SPIControl.
   * Private to force users to use the Singleton.
   **/
  SPIControl();

  unsigned long WriteAndRead(uint8_t * data, unsigned len, bool KeepHigh);
  unsigned long WriteAndRead(uint8_t data);
  //std::vector<uint8_t> Read(uint8_t size);


  //We might move this to another library
  std::vector<uint8_t> EditConfReg(std::vector<uint8_t> OrigRegVec ,std::string RegName, uint8_t NewVal ); 
    
  bool m_verbose;

  int32_t m_fd;
  std::string m_device_name;
  uint32_t m_speed;
  uint32_t m_bits_per_word_wr;
  uint32_t m_bits_per_word_rd;
  uint32_t m_spi_mode;
  uint8_t m_sampling_rate;
  
  static SPIControl * m_me; //!< singleton pointer to be returned by SPIControl::GetInstance


  CS5523 m_ADC;

  unsigned m_maxTries;
  std::mutex m_lock;

};

#endif
