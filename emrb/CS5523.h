#ifndef CS5523_H
#define CS5523_H


#include <string>
#include <vector>



class CS5523 {
 

 private:
  uint8_t m_RegisterMap[39]={0};
  
 public:

  struct Register {
    uint8_t * val;
    uint8_t size; //In Bytes
    uint8_t RegSet; //RSB
    uint8_t ChnSet; //channel select bit 
  };


  struct Field {
    uint8_t offset;
    uint8_t size;
    std::string name;
    const Register * reg;
  };


  
  
  void SetField(const Field field, uint32_t data);
  uint32_t GetField(const Field field);

  std::vector<uint8_t> GetCmd_WriteReg(const Register reg);
  std::vector<uint8_t> GetCmd_ReadReg(const Register reg);
  
  std::vector<uint8_t> GetCmd_PerfConv(uint8_t CSRP);
  std::vector<uint8_t> GetCmd_PerfCalib(uint8_t CSRP, uint8_t CCB);

  std::vector<uint8_t> GetCmd_SYNC1();
  std::vector<uint8_t> GetCmd_SYNC0();
  std::vector<uint8_t> GetCmd_NULL();



  
  
  //Memory of internal registers



  const Register m_reg_config = { &m_RegisterMap[0],3, 3, 0 };
  const Register m_reg_chanSetup = { &m_RegisterMap[3],12, 5, 0};
  const Register m_reg_offset[4] = { { &m_RegisterMap[15],3,1,0},
				     { &m_RegisterMap[18],3,1,1},
				     { &m_RegisterMap[21],3,1,2},
				     { &m_RegisterMap[24],3,1,3}};
  const Register m_reg_gain[4] =   { { &m_RegisterMap[27],3,2,0},
				     { &m_RegisterMap[30],3,2,1},
				     { &m_RegisterMap[33],3,2,2},
				     { &m_RegisterMap[36],3,2,3}};
 
  
  //Configuration Fields
  const Field CHOP_FREQUENCY_SELECT = {20,2,"ChopFrequencySelect",&m_reg_config};
  const Field MULTIPLE_CONVERSION= {18,1,"MultipleConversion",&m_reg_config};
  const Field LOOP= {17,1,"Loop",&m_reg_config};
  const Field READ_CONVERT = {16,1,"ReadConvert",&m_reg_config};
  const Field DEPTH_POINTER= {12,4,"DepthPointer",&m_reg_config};
  const Field POWER_SAVE_SELECT= {11,1,"PowerSaveSelect",&m_reg_config};
  const Field PUMP_DISABLE= {10,1,"PumpDisable",&m_reg_config};
  const Field RUN = {9,1,"Run",&m_reg_config};
  const Field LOW_POWER_MODE = {8,1,"LowPowerMode",&m_reg_config};
  const Field RESET_SYSTEM= {7,1,"ResetSystem",&m_reg_config};
  const Field RESET_VALID= {6,1,"ResetValid",&m_reg_config};
  const Field OSCILLATION_DETECT= {5,1,"OscillationDetect",&m_reg_config};
  const Field OVERRANGE_FLAG = {4,1,"OverrangeFlag",&m_reg_config};


  //Chanel setup Fields is 5 (1 0 1) // Still confusing
  const Field LATCH_OUTPUT_1 = {94,2,"LatchOutput_1",&m_reg_chanSetup};
  const Field CHANNEL_SELECT_1 = {91,3,"ChannelSelect_1",&m_reg_chanSetup};
  const Field WORD_RATE_1 = {88,3,"WordRate_1",&m_reg_chanSetup};
  const Field GAIN_BITS_1 = {85,3,"GainBits_1",&m_reg_chanSetup};
  const Field UNIPOLAR_MODE_1 = {84,1,"UnipolarMode_1",&m_reg_chanSetup};

  const Field LATCH_OUTPUT_2 = {82,2,"LatchOutput_2",&m_reg_chanSetup};
  const Field CHANNEL_SELECT_2 = {79,3,"ChannelSelect_2",&m_reg_chanSetup};
  const Field WORD_RATE_2 = {76,3,"WordRate_2",&m_reg_chanSetup};
  const Field GAIN_BITS_2 = {73,3,"GainBits_2",&m_reg_chanSetup};
  const Field UNIPOLAR_MODE_2 = {72,1,"UnipolarMode_2",&m_reg_chanSetup};

  const Field LATCH_OUTPUT_3 = {70,2,"LatchOutput_3",&m_reg_chanSetup};
  const Field CHANNEL_SELECT_3 = {67,3,"ChannelSelect_3",&m_reg_chanSetup};
  const Field WORD_RATE_3 = {64,3,"WordRate_3",&m_reg_chanSetup};
  const Field GAIN_BITS_3 = {61,3,"GainBits_3",&m_reg_chanSetup};
  const Field UNIPOLAR_MODE_3 = {60,1,"UnipolarMode_3",&m_reg_chanSetup};

  const Field LATCH_OUTPUT_4 = {58,2,"LatchOutput_4",&m_reg_chanSetup};
  const Field CHANNEL_SELECT_4 = {55,3,"ChannelSelect_4",&m_reg_chanSetup};
  const Field WORD_RATE_4 = {52,3,"WordRate_4",&m_reg_chanSetup};
  const Field GAIN_BITS_4 = {49,3,"GainBits_4",&m_reg_chanSetup};
  const Field UNIPOLAR_MODE_4 = {48,1,"UnipolarMode_4",&m_reg_chanSetup};

  const Field LATCH_OUTPUT_5 = {46,2,"LatchOutput_5",&m_reg_chanSetup};
  const Field CHANNEL_SELECT_5 = {43,3,"ChannelSelect_5",&m_reg_chanSetup};
  const Field WORD_RATE_5 = {40,3,"WordRate_5",&m_reg_chanSetup};
  const Field GAIN_BITS_5 = {37,3,"GainBits_5",&m_reg_chanSetup};
  const Field UNIPOLAR_MODE_5 = {36,1,"UnipolarMode_5",&m_reg_chanSetup};
  
  const Field LATCH_OUTPUT_6 = {34,2,"LatchOutput_6",&m_reg_chanSetup};
  const Field CHANNEL_SELECT_6 = {31,3,"ChannelSelect_6",&m_reg_chanSetup};
  const Field WORD_RATE_6 = {28,3,"WordRate_6",&m_reg_chanSetup};
  const Field GAIN_BITS_6 = {25,3,"GainBits_6",&m_reg_chanSetup};
  const Field UNIPOLAR_MODE_6 = {24,1,"UnipolarMode_6",&m_reg_chanSetup};

  const Field LATCH_OUTPUT_7 = {22,2,"LatchOutput_7",&m_reg_chanSetup};
  const Field CHANNEL_SELECT_7 = {19,3,"ChannelSelect_7",&m_reg_chanSetup};
  const Field WORD_RATE_7 = {16,3,"WordRate_7",&m_reg_chanSetup};
  const Field GAIN_BITS_7 = {13,3,"GainBits_7",&m_reg_chanSetup};
  const Field UNIPOLAR_MODE_7 = {12,1,"UnipolarMode_7",&m_reg_chanSetup};

  const Field LATCH_OUTPUT_8 = {10,2,"LatchOutput_8",&m_reg_chanSetup};
  const Field CHANNEL_SELECT_8 = {7,3,"ChannelSelect_8",&m_reg_chanSetup};
  const Field WORD_RATE_8 = {4,3,"WordRate_8",&m_reg_chanSetup};
  const Field GAIN_BITS_8 = {1,3,"GainBits_8",&m_reg_chanSetup};
  const Field UNIPOLAR_MODE_8 = {0,1,"UnipolarMode_8",&m_reg_chanSetup};



  //Offset and Gain Fields
  const Field CH_0_SIGN = {23,1,"CH0_SIGN",&m_reg_offset[0]};
  const Field CH_0_OFFSET = {0,23,"CH0_OFFSET",&m_reg_offset[0]};
  const Field CH_0_GAIN = {0,24,"CH0_GAIN",&m_reg_gain[0]};

  const Field CH_1_SIGN = {23,1,"CH1_SIGN",&m_reg_offset[1]};
  const Field CH_1_OFFSET = {0,23,"CH1_OFFSET",&m_reg_offset[1]};
  const Field CH_1_GAIN = {0,24,"CH1_GAIN",&m_reg_gain[1]};

  const Field CH_2_SIGN = {23,1,"CH2_SIGN",&m_reg_offset[2]};
  const Field CH_2_OFFSET = {0,23,"CH2_OFFSET",&m_reg_offset[2]};
  const Field CH_2_GAIN = {0,24,"CH2_GAIN",&m_reg_gain[2]};


  const Field CH_3_SIGN = {23,1,"CH3_SIGN",&m_reg_offset[3]};
  const Field CH_3_OFFSET = {0,23,"CH3_OFFSET",&m_reg_offset[3]};
  const Field CH_3_GAIN = {0,24,"CH3_GAIN",&m_reg_gain[3]};



  const Field LATCH_OUTPUT[8]={
    LATCH_OUTPUT_1,
    LATCH_OUTPUT_2,
    LATCH_OUTPUT_3,
    LATCH_OUTPUT_4,
    LATCH_OUTPUT_5,
    LATCH_OUTPUT_6,
    LATCH_OUTPUT_7,
    LATCH_OUTPUT_8};

  const Field CHANNEL_SELECT[8]={
    CHANNEL_SELECT_1,
    CHANNEL_SELECT_2,
    CHANNEL_SELECT_3,
    CHANNEL_SELECT_4,
    CHANNEL_SELECT_5,
    CHANNEL_SELECT_6,
    CHANNEL_SELECT_7,
    CHANNEL_SELECT_8};

  const Field WORD_RATE[8]={
    WORD_RATE_1,
    WORD_RATE_2,
    WORD_RATE_3,
    WORD_RATE_4,
    WORD_RATE_5,
    WORD_RATE_6,
    WORD_RATE_7,
    WORD_RATE_8};

  const Field GAIN_BITS[8]={
    GAIN_BITS_1,
    GAIN_BITS_2,
    GAIN_BITS_3,
    GAIN_BITS_4,
    GAIN_BITS_5,
    GAIN_BITS_6,
    GAIN_BITS_7,
    GAIN_BITS_8};

  const Field UNIPOLAR_MODE[8]={
    UNIPOLAR_MODE_1,
    UNIPOLAR_MODE_2,
    UNIPOLAR_MODE_3,
    UNIPOLAR_MODE_4,
    UNIPOLAR_MODE_5,
    UNIPOLAR_MODE_6,
    UNIPOLAR_MODE_7,
    UNIPOLAR_MODE_8};

  const Field CH_SIGN[4] = {CH_0_SIGN,CH_1_SIGN,CH_2_SIGN,CH_3_SIGN};
  const Field CH_OFFSET[4] = {CH_0_OFFSET,CH_1_OFFSET,CH_2_OFFSET,CH_3_OFFSET};
  const Field CH_GAIN[4] = {CH_0_GAIN,CH_1_GAIN,CH_2_GAIN,CH_3_GAIN};

  



  



  
};



#endif
